package com.projectfuture.repairwebapp;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.RepairService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootApplication
public class  RepairWebAppApplication implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Autowired
	private PropertyOwnerService propertyOwnerService;

	@Autowired
	private RepairService repairService;

	public static void main(String[] args) {
		SpringApplication.run(RepairWebAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("=============================");
		logger.info("====Finding Owner by Id====");
		PropertyOwner foundOwner = propertyOwnerService.findPropertyOwner(1L);
		logger.info(foundOwner.toString());
		logger.info("=============================");
		logger.info("=============================");

		logger.info("=============================");
		logger.info("====Finding Owner by SSN====");
		PropertyOwner foundOwnerSsn = propertyOwnerService.findPropertyOwnerBySsn("816489573");
		logger.info(foundOwnerSsn.toString());
		logger.info("=============================");
		logger.info("=============================");


		logger.info("=============================");
		logger.info("====Finding Owner by email====");
		PropertyOwner foundOwnerEmail = propertyOwnerService.findPropertyOwnerByEmail("k.kritikos@gmail.com");
		logger.info(foundOwnerEmail.toString());
		logger.info("=============================");
		logger.info("=============================");


		logger.info("=============================");
		logger.info("====Finding Repair by Id====");
		Repair foundRepair = repairService.findRepair(1L);
		logger.info(foundRepair.toString());
		logger.info("=============================");
		logger.info("=============================");

		logger.info("=============================");
		logger.info("====Finding Repair by Owner SSN====");
		List<Repair> foundRepairBySsn = repairService.findRepairByPropertyOwnerSsn("123589467");
		for (Repair repair : foundRepairBySsn) { logger.info(repair.toString()); }
		logger.info("=============================");
		logger.info("=============================");

		logger.info("=============================");
		logger.info("====Finding Repair by Date====");
		LocalDate localDate = LocalDate.parse("2019-12-12", DATE_TIME_FORMATTER);
		List<Repair> foundRepairByDate = repairService.findRepairByDate(localDate);
		for (Repair repair : foundRepairByDate) { logger.info(repair.toString()); }
		logger.info("=============================");
		logger.info("=============================");

		logger.info("=============================");
		logger.info("====Finding Repair by Date Between====");
		LocalDate fromDate = LocalDate.parse("2019-12-22", DATE_TIME_FORMATTER);
		LocalDate untilDate = LocalDate.parse("2019-12-26", DATE_TIME_FORMATTER);
		List<Repair> foundRepairByDateBetween = repairService.findRepairByDateBetweenOrderByDate(fromDate, untilDate);
		for (Repair repair : foundRepairByDateBetween) { logger.info(repair.toString()); }
		logger.info("=============================");
		logger.info("=============================");

	}
}
