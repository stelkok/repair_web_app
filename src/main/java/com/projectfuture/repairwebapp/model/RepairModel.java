package com.projectfuture.repairwebapp.model;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.enums.PropertyType;
import com.projectfuture.repairwebapp.enums.RepairStatus;
import com.projectfuture.repairwebapp.enums.RepairType;

public class RepairModel {

    private long id;
    private String date;
    private RepairStatus repairStatus;
    private RepairType repairType;
    private float cost;
    private String address;
    private String ownerName;
    private PropertyOwner propertyOwner;
    private PropertyType propertyType;
    private String description;

    public RepairModel(){ }

    public RepairModel(long id, String date, RepairStatus repairStatus, RepairType repairType, float cost, String address, String ownerName, PropertyType propertyType, PropertyOwner propertyOwner, String description) {
        this.id = id;
        this.date = date;
        this.repairStatus = repairStatus;
        this.repairType = repairType;
        this.cost = cost;
        this.address = address;
        this.ownerName = ownerName;
        this.propertyType = propertyType;
        this.propertyOwner = propertyOwner;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RepairStatus getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatus repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairType getRepairType() {
        return repairType;
    }

    public void setRepairType(RepairType repairType) {
        this.repairType = repairType;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public PropertyType getPropertyType() { return propertyType; }

    public void setPropertyType(PropertyType propertyType) { this.propertyType = propertyType; }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
