package com.projectfuture.repairwebapp.repository;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.domain.Repair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface RepairRepository extends JpaRepository<Repair, Long> {

    List<Repair> findRepairByDate(LocalDate date);

    List<Repair> findRepairByDateBetweenOrderByDate(LocalDate from, LocalDate until);

    List<Repair> findRepairByPropertyOwner_Id(Long ownerId);

    List<Repair> findRepairByDateIsAfterOrderByDateAsc(LocalDate date);

    Repair save(Repair repair);

    List<Repair> findRepairByPropertyOwnerEmail(String name);

    void deleteRepairById(long id);

}
