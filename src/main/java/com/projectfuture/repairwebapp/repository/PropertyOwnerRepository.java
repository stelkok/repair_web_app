package com.projectfuture.repairwebapp.repository;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PropertyOwnerRepository extends JpaRepository<PropertyOwner, Long> {

    Optional<PropertyOwner> findPropertyOwnerBySsn(String ssn);

    Optional<PropertyOwner> findPropertyOwnerByFirstName(String name);

    Optional<PropertyOwner> findPropertyOwnerByEmail(String email);

    Optional<PropertyOwner> findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName);

    PropertyOwner save(PropertyOwner propertyOwner);

    boolean existsOwnerBySsn(String ssn);

    boolean existsOwnerByPhoneNumber(String phoneNumber);

    boolean existsOwnerByEmail(String email);

    void deleteById(Long id);


}
