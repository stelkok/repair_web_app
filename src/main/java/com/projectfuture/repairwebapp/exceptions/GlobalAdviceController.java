package com.projectfuture.repairwebapp.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice(basePackages = {
        "com.projectfuture.repairwebapp"
})
    
public class GlobalAdviceController {


    @ModelAttribute
    public void init(ModelMap modelMap){
        modelMap.addAttribute("title","test");
    }
}
