package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.mappers.PropertyOwnerToPropertyOwnerModelMapper;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PropertyOwnerServiceImpl implements PropertyOwnerService{

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Autowired
    private PropertyOwnerToPropertyOwnerModelMapper mapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public PropertyOwner findPropertyOwner(Long id) {
        return propertyOwnerRepository.findById(id).get();
    }

    @Override
    public List<PropertyOwner> getAllPropertyOwners() {
        return propertyOwnerRepository.findAll();
    }

    @Override
    public PropertyOwner findPropertyOwnerBySsn(String ssn) {
        return propertyOwnerRepository.findPropertyOwnerBySsn(ssn).get();
    }

    @Override
    public PropertyOwner findPropertyOwnerByEmail(String email) {
        return propertyOwnerRepository.findPropertyOwnerByEmail(email).get();
    }

    @Override
    public PropertyOwner findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName) {
        return propertyOwnerRepository.findPropertyOwnerByFirstNameAndLastName(firstName, lastName).get();
    }

    @Override
    public PropertyOwner findPropertyOwnerByFirstName(String name){

        return propertyOwnerRepository.findPropertyOwnerByFirstName(name).get();
    }

    @Override
    public PropertyOwner createPropertyOwner(PropertyOwnerModel propertyOwnerModel) {
        PropertyOwner newPropertyOwner = new PropertyOwner();
        newPropertyOwner.setSsn(propertyOwnerModel.getSsn());
        newPropertyOwner.setFirstName(propertyOwnerModel.getFirstName());
        newPropertyOwner.setLastName(propertyOwnerModel.getLastName());
        newPropertyOwner.setAddress(propertyOwnerModel.getAddress());
        newPropertyOwner.setPhoneNumber(propertyOwnerModel.getPhoneNumber());
        newPropertyOwner.setEmail(propertyOwnerModel.getEmail());
        newPropertyOwner.setPassword(passwordEncoder.encode(propertyOwnerModel.getPassword()));
        newPropertyOwner.setRoleType(propertyOwnerModel.getRoleType());
        return propertyOwnerRepository.save(newPropertyOwner);
    }

    @Override
    public PropertyOwner updatePropertyOwner(PropertyOwnerModel propertyOwnerModel) {
        PropertyOwner originalPropertyOwner = propertyOwnerRepository.findById(propertyOwnerModel.getId()).get();
        originalPropertyOwner.setSsn(propertyOwnerModel.getSsn());
        originalPropertyOwner.setFirstName(propertyOwnerModel.getFirstName());
        originalPropertyOwner.setLastName(propertyOwnerModel.getLastName());
        originalPropertyOwner.setAddress(propertyOwnerModel.getAddress());
        originalPropertyOwner.setPhoneNumber(propertyOwnerModel.getPhoneNumber());
        originalPropertyOwner.setEmail(propertyOwnerModel.getEmail());
        originalPropertyOwner.setPassword(passwordEncoder.encode(propertyOwnerModel.getPassword()));
        return propertyOwnerRepository.save(originalPropertyOwner);
    }

    @Override
    public void deleteById(Long id) {
        propertyOwnerRepository.deleteById(id);
    }


}
