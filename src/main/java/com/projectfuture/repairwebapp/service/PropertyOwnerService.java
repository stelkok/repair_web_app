package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;

import java.util.List;

public interface PropertyOwnerService {

    PropertyOwner findPropertyOwner(Long id);

    List<PropertyOwner> getAllPropertyOwners();

    PropertyOwner findPropertyOwnerBySsn(String ssn);

    PropertyOwner findPropertyOwnerByFirstName(String name);

    PropertyOwner findPropertyOwnerByEmail(String email);

    PropertyOwner findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName);

    PropertyOwner createPropertyOwner(PropertyOwnerModel propertyOwnerModel);

    PropertyOwner updatePropertyOwner(PropertyOwnerModel propertyOwnerModel);

    void deleteById(Long id);


}
