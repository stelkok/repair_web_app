package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.mappers.RepairToRepairModelMapper;
import com.projectfuture.repairwebapp.model.RepairModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import com.projectfuture.repairwebapp.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Transactional
public class RepairServiceImpl implements RepairService {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Override
    public Repair findRepair(Long id) {
        return repairRepository.findById(id).get();
    }

    @Override
    public List<Repair> getAllRepairs() {
        return repairRepository.findAll();
    }

    @Override
    public List<Repair> findRepairByDate(LocalDate date) {

        return repairRepository.findRepairByDate(date);
    }

    @Override
    public List<Repair> findRepairByDateBetweenOrderByDate(LocalDate from, LocalDate until) {
        return repairRepository.findRepairByDateBetweenOrderByDate(from, until);
    }

    @Override
    public List<Repair> findRepairByPropertyOwnerSsn(String ssn) {
        Long ownerId = propertyOwnerRepository.findPropertyOwnerBySsn(ssn).get().getId();
        return repairRepository.findRepairByPropertyOwner_Id(ownerId);
    }

    @Override
    public List<Repair> findRepairByDateIsAfterOrderByDateAsc(LocalDate date) {
        List<Repair> repairs = repairRepository.findRepairByDateIsAfterOrderByDateAsc(date);
        int repairsAmount = repairs.size();
        if(repairsAmount<10){
            repairs = repairs.subList(0,repairsAmount);
        } else {
            repairs = repairs.subList(0,10);
        }

        return repairs;
    }

    @Override
    public Repair createProperty(RepairModel RepairModel) {
        Repair originalRepair = new Repair();
        originalRepair.setDate(LocalDate.parse(RepairModel.getDate(), DATE_TIME_FORMATTER));
        originalRepair.setRepairStatus(RepairModel.getRepairStatus());
        originalRepair.setRepairType(RepairModel.getRepairType());
        originalRepair.setCost(RepairModel.getCost());
        originalRepair.setAddress(RepairModel.getAddress());
        originalRepair.setPropertyType(RepairModel.getPropertyType());
        originalRepair.setPropertyOwner(RepairModel.getPropertyOwner());
        originalRepair.setDescription(RepairModel.getDescription());
        return repairRepository.save(originalRepair);
    }

    @Override
    public Repair updateRepair(RepairModel RepairModel) {
        Repair originalRepair = repairRepository.findById(RepairModel.getId()).get();
        originalRepair.setId(RepairModel.getId());
        originalRepair.setDate(LocalDate.parse(RepairModel.getDate(), DATE_TIME_FORMATTER));
        originalRepair.setRepairStatus(RepairModel.getRepairStatus());
        originalRepair.setRepairType(RepairModel.getRepairType());
        originalRepair.setCost(RepairModel.getCost());
        originalRepair.setAddress(RepairModel.getAddress());
        originalRepair.setPropertyType(RepairModel.getPropertyType());
        originalRepair.setPropertyOwner(RepairModel.getPropertyOwner());
        originalRepair.setDescription(RepairModel.getDescription());
        return repairRepository.save(originalRepair);
    }


    @Override
    public List<Repair> findRepairByPropertyOwnerEmail(String name){
        PropertyOwner owner= propertyOwnerRepository.findPropertyOwnerByEmail(name).get();
        return repairRepository.findRepairByPropertyOwner_Id(owner.getId());

    }

    @Override
    public void deleteRepairById(long id) {
        repairRepository.deleteRepairById(id);
    }
}
