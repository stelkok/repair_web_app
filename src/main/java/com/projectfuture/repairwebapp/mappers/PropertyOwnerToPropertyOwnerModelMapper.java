package com.projectfuture.repairwebapp.mappers;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import org.springframework.stereotype.Component;

@Component
public class PropertyOwnerToPropertyOwnerModelMapper {

    public PropertyOwnerModel mapToPropertyOwnerModel(PropertyOwner propertyOwner){
        PropertyOwnerModel model = new PropertyOwnerModel();
        model.setId(propertyOwner.getId());
        model.setFirstName(propertyOwner.getFirstName());
        model.setLastName(propertyOwner.getLastName());
        model.setAddress(propertyOwner.getAddress());
        model.setEmail(propertyOwner.getEmail());
        model.setSsn(propertyOwner.getSsn());
        return model;
    }

}
