package com.projectfuture.repairwebapp.mappers;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.model.RepairModel;
import org.springframework.stereotype.Component;

@Component
public class RepairToRepairModelMapper {

    public RepairModel mapToRepairModel(Repair repair){
        RepairModel model = new RepairModel();
        model.setAddress(repair.getAddress());
        model.setId(repair.getId());
        model.setCost(repair.getCost());
        model.setDate(repair.getDate().toString());
        model.setDescription(repair.getDescription());
        model.setPropertyOwner(repair.getPropertyOwner());
        model.setPropertyType(repair.getPropertyType());
        model.setRepairStatus(repair.getRepairStatus());
        model.setRepairType(repair.getRepairType());
        return model;
    }

}
