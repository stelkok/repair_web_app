package com.projectfuture.repairwebapp.enums;

public enum PropertyType {
    DEFAULT("Undefined"),
    BLOCK_OF_FLATS("Block of flats"),
    APARTMENT("Apartment"),
    HOUSE("House"),
    MAISONETTE("Maisonette"),
    SHOP("Shop");

    private String propertyType;

    PropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyType() {
        return propertyType;
    }
}
