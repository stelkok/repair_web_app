package com.projectfuture.repairwebapp.enums;

public enum RepairType {
    DEFAULT("Undefined"),
    PAINT_JOB("Paint job"),
    ELECTRICAL_WORK("Electrical work"),
    PLUMBING("Plumbing"),
    FRAMING("Framing");

    private String repairType;

    RepairType(String repairType) {
        this.repairType = repairType;
    }

    public String getRepairType() {
        return repairType;
    }
}
