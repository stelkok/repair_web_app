package com.projectfuture.repairwebapp.enums;

public enum RoleType {

    USER("User"),
    ADMIN("Admin");

    private String roleType;

    RoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleType() {
        return roleType;
    }

}
