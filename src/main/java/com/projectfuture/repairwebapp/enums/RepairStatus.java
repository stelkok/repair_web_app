package com.projectfuture.repairwebapp.enums;

public enum RepairStatus {
    DEFAULT("Pending"),
    PENDING("Pending"),
    UNDERWAY("Underway"),
    COMPLETED("Completed");

    private String repairStatus;

    RepairStatus(String repairStatus) {
        this.repairStatus = repairStatus;
    }

    public String getRepairStatus() {
        return repairStatus;
    }

}
