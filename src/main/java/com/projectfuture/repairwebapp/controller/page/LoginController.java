package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.forms.LoginForm;
import com.projectfuture.repairwebapp.validators.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LoginController {

    private static final String LOGIN_FORM = "form-login";

    @Autowired
    private LoginValidator loginValidator;

    @InitBinder(LOGIN_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(loginValidator);
    }

    @GetMapping(value = "/login")
    public String register(Model model) {
        model.addAttribute(LOGIN_FORM, new LoginForm());
        return "login";
    }

}
