package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    RepairService repairService;

    @Autowired
    PropertyOwnerService propertyOwnerService;

    @GetMapping("/admin/home")
    public String getHomePage(Model model){
        List<Repair> repairs = repairService.findRepairByDateIsAfterOrderByDateAsc(LocalDate.now());
        model.addAttribute("repairs", repairs);
        return "home-for-admin";
        }

    @GetMapping("/user/homepage")
    public String getUserHomePage(Model model){
        List<Repair> repairs = repairService.findRepairByPropertyOwnerEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("repairs", repairs);
        return "home-for-user";
    }


}
