package com.projectfuture.repairwebapp.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class ErrorController  {

    @GetMapping(value = "/generic")
    public String errorPage(Model model) {
        return "tables/error";
    }

    @GetMapping(value = "/access-denied")
    public String accessDeniedPage(Model model) {
         return "/tables/access-error";
     }


}
