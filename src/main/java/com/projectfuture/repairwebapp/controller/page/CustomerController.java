package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase;

@Controller
//@RequestMapping("admin")
public class CustomerController {

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @GetMapping("/admin/customers")
    public String getCustomers(Model model){
        List<PropertyOwner> propertyOwners = propertyOwnerService.getAllPropertyOwners();
        model.addAttribute("owners", propertyOwners);
        return "customers";
    }

    @GetMapping({"/admin/create-customer"})
    public String getCustomerCreatePage(Model model){
        PropertyOwner newPropertyOwner = new PropertyOwner();
        model.addAttribute("propertyOwner", newPropertyOwner);
        return "create-customer";
    }

    @PostMapping ({"/admin/create-customer"})
    public String createNewCustomer(PropertyOwnerModel model, Model model2){
        PropertyOwner errorPropertyOwner = new PropertyOwner();

        boolean ssnExists= propertyOwnerRepository.existsOwnerBySsn(model.getSsn());
        boolean phoneNumberExists=propertyOwnerRepository.existsOwnerByPhoneNumber(model.getPhoneNumber());
        boolean emailExists=propertyOwnerRepository.existsOwnerByEmail(model.getEmail());

        if(ssnExists || phoneNumberExists || emailExists){
            if(ssnExists) errorPropertyOwner.setSsn(model.getSsn());
            if(phoneNumberExists) errorPropertyOwner.setPhoneNumber(model.getPhoneNumber());
            if(emailExists) errorPropertyOwner.setEmail(model.getEmail());
            model2.addAttribute("errorOwner", errorPropertyOwner);
            return "duplicate-entry-error";
        }
        propertyOwnerService.createPropertyOwner(model);
        return "redirect:/admin/customers";
    }

    @GetMapping({"/admin/edit-customer/{id}"})
    public String getCustomerEditPage(@PathVariable Long id, Model model){
        PropertyOwner owner = propertyOwnerService.findPropertyOwner(id);
        model.addAttribute("owner", owner);
        return "edit-customer";
    }

    @PostMapping({"/admin/edit-customer/{id}"})
    public String returnFromCustomerEditPage(@PathVariable Long id, PropertyOwnerModel model){
        model.setId(id);
        propertyOwnerService.updatePropertyOwner(model);
        return "redirect:/admin/customers";
    }

    @PostMapping({"/admin/remove-customer/{id}"})
    public String getCustomerRemovePage(@PathVariable Long id){
        propertyOwnerService.deleteById(id);
        return "redirect:/admin/customers";
    }

    @GetMapping({"/admin/search-customers"})
    public String getSearchCustomerPage(Long id, Model model){
        return "search-customers";
    }

    @PostMapping({"/admin/search-customers"})
    public String getCustomerSearchResult(@RequestParam(name = "ssn") String ssn,
                                          @RequestParam(name = "email") String email,
                                          Model model){

        boolean ssnExists= propertyOwnerRepository.existsOwnerBySsn(ssn);
        boolean emailExists=propertyOwnerRepository.existsOwnerByEmail(email);

        PropertyOwner owner = null;
        if (!ssn.equals("")){
            if (!ssnExists){
                return "customer-search-error";
            }
            owner = propertyOwnerService.findPropertyOwnerBySsn(ssn);
        }else if (!email.equals("")){
            if (!emailExists){
                return "customer-search-error";
            }
            owner = propertyOwnerService.findPropertyOwnerByEmail(email);
        }
        if(!owner.equals(null)){
            model.addAttribute("owner", owner);
        }

        model.addAttribute("ssn",ssn);
        model.addAttribute("email",email);

        return "search-customers";
    }


}
