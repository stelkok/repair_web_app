package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.enums.PropertyType;
import com.projectfuture.repairwebapp.enums.RepairStatus;
import com.projectfuture.repairwebapp.enums.RepairType;
import com.projectfuture.repairwebapp.model.RepairModel;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RepairController {

    @Autowired
    private RepairService repairService;

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    @GetMapping("/admin/repairs")
    public String getRepairs(Model model){
        List<Repair> repairs = repairService.getAllRepairs();
        model.addAttribute("repairs", repairs);
        return "repairs";
    }

    @GetMapping({"/admin/create-repair"})
    public String getRepairCreatePage(Model model){
        return "create-repair";
    }

    @PostMapping({"/admin/create-repair"})
    public String saveRepair(RepairModel repairModel){
        String firstName = repairModel.getOwnerName().split(" ")[0];
        String lastName = repairModel.getOwnerName().split(" ")[1];

        repairModel.setPropertyOwner(propertyOwnerService.findPropertyOwnerByFirstNameAndLastName(firstName, lastName));
        repairService.createProperty(repairModel);
        return "redirect:/admin/repairs";
    }

    @GetMapping({"/admin/edit-repair/{id}"})
    public String getRepairEditPage(@PathVariable Long id, Model model){
        Repair repair = repairService.findRepair(id);
        model.addAttribute("repair", repair);
//        model.addAttribute("status", RepairStatus.values());
//        model.addAttribute("type", RepairType.values());
//        model.addAttribute("typeOfProperty", PropertyType.values());
        return "edit-repair";
    }

    @PostMapping("/admin/edit-repair/{id}")
    public String updateRepair(@PathVariable Long id, RepairModel repairModel){
        String firstName = repairModel.getOwnerName().split(" ")[0];
        String lastName = repairModel.getOwnerName().split(" ")[1];

        repairModel.setPropertyOwner(propertyOwnerService.findPropertyOwnerByFirstNameAndLastName(firstName, lastName));
        repairService.updateRepair(repairModel);
        return "redirect:/admin/repairs";
    }

    @PostMapping({"/admin/remove-repair/{id}"})
    public String deleteRepair(@PathVariable Long id){
        repairService.deleteRepairById(id);
        return "redirect:/admin/repairs";
    }

    @GetMapping({"/admin/search-repair"})
    public String getRepairsSearchPage(Long id, Model model){
        return "search-repairs";
    }

    @PostMapping({"/admin/search-repair"})
    public String getRepairSearchResult(@RequestParam(name = "fromDate") String fromDateString,
                                  @RequestParam(name = "untilDate") String untilDateString,
                                  @RequestParam(name = "ownerSsn") String ownerSsn,
                                  Model model){

        LocalDate fromDate = LocalDate.parse(fromDateString, DATE_TIME_FORMATTER);
        LocalDate untilDate = LocalDate.parse(untilDateString, DATE_TIME_FORMATTER);
        List<Repair> repairsByOwnerSsn = repairService.findRepairByPropertyOwnerSsn(ownerSsn);
        List<Repair> repairsByDate = repairService.findRepairByDateBetweenOrderByDate(fromDate, untilDate);

        List<Repair> repairs = new ArrayList<>();

        for (Repair r : repairsByDate) {
            if (repairsByOwnerSsn.contains(r)) {
                repairs.add(r);
            }
        }

        if (!repairs.isEmpty()){
            model.addAttribute("repairs", repairs);
        }
        model.addAttribute("fromDate",fromDateString);
        model.addAttribute("untilDate",untilDateString);
        model.addAttribute("ownerSsn",ownerSsn);

        return "search-repairs";
    }
}
