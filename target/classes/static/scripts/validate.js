jQuery(function ($) {
	jQuery.validator.addMethod("isfloat", function(value, element) {
        return this.optional(element) || (/^([1-9][0-9]*\.[0-9]{1,2})$/.test(value) || /^([1-9][0-9]*)$/.test(value));
    }, "* Please enter a valid cost");

    if($('#cost').length > 0){
        $('#cost').val($('#cost').val().replace(",","."));
    }

	$('#owner-validation').validate(
		{
		rules:
			{
				ssn: { required: true, digits: true, minlength: 9, maxlength: 9 },
				firstName: { required: true, maxlength: 255 },
				lastName: {required: true, maxlength: 255 },
				phoneNumber: {required: true, digits: true},
				address: {required: true, maxlength: 255},
				email: { required: true, email: true, maxlength: 255},
				password: { required: true }
			},
		messages:
			{
				ssn: { required: 'Please enter your SSN', digits: 'SSN can only contain digits', minlength: 'SSN must be 9 digits long', maxlength: 'SSN must be 9 digits long'},
				firstName: { required: 'Please enter your first name', maxlength: 'Your first name cannot have more than 255 characters'},
				lastName: { required: 'Please enter your last name', maxlength: 'Your last name cannot have more than 255 characters'},
				phoneNumber: {required: 'Please enter your phone number', digits: 'Phone number can only contain digits'},
				address: {required: 'Please enter your address', maxlength: 'Your address cannot have more than 255 characters'},
				email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters'},
				password: { required: 'Please enter a password'}
			}
		}
	);

	$('#repair-validation').validate(
		{
		rules:
			{
				date: { required: true },
				cost: { required: true, isfloat: true},
				address: { required: true, maxlength: 255 },
				ownerName: { required: true }
			},
		messages:
			{
				date: { required: 'Please enter a date' },
				cost: { required: 'Please enter a cost', isfloat: 'Please enter a valid cost'},
				address: { required: 'Please enter an address', maxlength: 'Address can have up to 255 characters'},
				ownerName: { required: 'Please enter a name for the property owner' }
			}
		}
	);

	$('#search-repair-validation').validate(
		{
			rules:
				{
					fromDate: { required: true },
					untilDate: { required: true },
					ownerSsn: { required: true, digits: true, minlength: 9, maxlength: 9 }
				},
			messages:
				{
					fromDate: { required: 'Please enter a starting date' },
					untilDate: { required: 'Please enter an ending date' },
					ownerSsn: { required: 'Please enter an SSN', digits: 'SSN must contain only digits', minlength: 'SSN must have 9 digits', maxlength: 'SSN must have 9 digits' }
				}
		}
	);

	$('#search-customer-ssn-validation').validate(
		{
			rules:
				{
					ssn: { required: true, digits: true, minlength: 9, maxlength: 9}
				},
			messages:
				{
					ssn: { required: "Please enter an SSN", digits: 'SSN must contain only digits', minlength: 'SSN must have 9 digits', maxlength: 'SSN must have 9 digits' },}
		}
	);

	$('#search-customer-email-validation').validate(
		{
			rules:
				{
					email: { required: true, email: true }
				},
			messages:
				{
					email: { required: "Please enter an email", email: 'Please enter a valid email'}
				}
		}
	);

	$('#removeCustomerModal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteForm').attr('action', `/admin/remove-customer/${id}`); //action takes place in modal instead of form
        $('.modal-title').html('Delete Customer');
        $('.modal-body').html('You are about to delete a customer with id: '+id+'<br/>All repair jobs from this customer will get deleted too.<br/>This action cannot be reversed...<br/>Do you want to proceed?');
    });

	$('#removeRepairModal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteForm').attr('action', `/admin/remove-repair/${id}`);  //action takes place in modal instead of form
        $('.modal-title').html('Delete Repair Job');
        $('.modal-body').html('You are about to delete a repair job with id: '+id+'<br>This action cannot be reversed...<br>Do you want to proceed?');
    });

    $('#promoModal').on('show.bs.modal', function (event) {
            $('.modal-title').html('Future Repairs');
            $('.modal-body').html('Thank you for choosing us!!!');
        });
});