The project’s goal was to develop an app for a construction/refurbishment company, giving the employees(admins) the ability to manage information about the customers,repairs
and real estates and to the customers(users) the ability to view the progress of each task they assigned to the company.

Regarding the test data, all passwords in the import.sql are hashed so in order to get access here is a list of the actual passwords:

123456
0123456
654321
000000
111111
kodikos1234
guest
root
toor123
password1
11071980
repairpass
mpainw
papa1507
eimaithea1



Check the inserts in the import.sql and match 1-1 with rows.